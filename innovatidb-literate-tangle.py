import os
from dotenv import load_dotenv
import pandas as pd
import psycopg2
from tabulate import tabulate


class InnovatiDB():
    """Database Class."""

    def __init__(self):
        """init class and connect to database. Credentials are to be provided by load_dotenv()."""
        self.conn = psycopg2.connect(dbname=os.getenv("DB_NAME"),
                                     user=os.getenv("DB_USER"),
                                     password=os.getenv("DB_PASSWORD"))
        self.cur = self.conn.cursor()

    def query(self, query):
        self.cur.execute(query)

    def commit(self):
        self.conn.commit()

    def fetchall(self):
        return self.cur.fetchall()

    def fetchall_flat(self):
        data = self.cur.fetchall()
        return [item for sublist in data for item in sublist]

    def rowcount(self):
        return self.cur.rowcount

    # Read query into pandas DataFrame
    def pd_read_sql(self, query):
        return pd.read_sql(query, self.conn)

    def close(self):
        self.cur.close()
        self.conn.close()

periods = {"3": ["1957-10-15", "1961-10-15"],
           "4": ["1961-10-17", "1965-10-17"],
           "5": ["1965-10-19", "1969-10-19"],
           "6": ["1969-10-20", "1972-12-13"],
           "7": ["1972-12-13", "1976-12-14"],
           "8": ["1976-12-14", "1980-11-04"],
           "9": ["1980-11-04", "1983-03-29"],
           "10": ["1983-03-29", "1987-02-18"],
           "11": ["1987-02-18", "1990-12-20"],
           "12": ["1990-12-20", "1994-11-10"],
           "13": ["1994-11-10", "1998-10-26"],
           "14": ["1998-10-26", "2002-10-17"],
           "15": ["2002-10-17", "2005-10-18"],
           "16": ["2005-10-18", "2009-10-27"],
           "17": ["2009-10-27", "2013-10-22"],
           "18": ["2013-10-22", "2017-10-24"]
           }


load_dotenv()
db = InnovatiDB()

for p in enumerate(periods):
    # query database for period using (arbitrary) threshold
    df_tmp = db.pd_read_sql(("""
   SELECT f.lemma AS "lemma",
          (count(f.lemma) filter (where periode = \'{0}\')
          /
          NULLIF((SELECT sum(woerter_anz)
                    FROM texte
                   WHERE periode = \'{0}\'),0)::decimal*100
                  ) AS "{0}"
     FROM texte t
FULL JOIN fundstellen f on t.id = f.text
    WHERE f.lemma IS NOT NULL
 GROUP BY f.lemma
   HAVING count(f.lemma) >= {1}
 ORDER BY "{0}" DESC;""").format(p[1], 1900))

    # Merge dataframes
    if p[0] == 0:
        df = df_tmp
    else:
        df = pd.merge(df, df_tmp, on='lemma')
db.close()


df.set_index('lemma', inplace=True)

# transpose dataframe
df_transposed = df.T

# Calculate correlation matrix using kendall's tau
corr_matrix = df_transposed.corr(method='kendall')

print(tabulate(corr_matrix, showindex=True, headers="keys", tablefmt='orgtbl', floatfmt=".10f"))
